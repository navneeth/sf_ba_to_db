#!/usr/bin/python
import argparse
import sys
import psycopg2, psycopg2.extras
import pprint
import json
import csv

def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('--homeId', dest='homeId', required=True)
    parser.add_argument('--positionCorrectionsFile', dest='positionCorrectionsFile', required=True, type=argparse.FileType('r'))
    parser.add_argument('--anglesCorrectionsFile', dest='anglesCorrectionsFile', required=True, type=argparse.FileType('r'))

    parsed_args, remainder = parser.parse_known_args(args)
    return parsed_args, remainder

from collections import defaultdict
pano_dict = defaultdict(list)

def readBA_AngleCorrections(f):
	content = f.readlines()
	# Return a list of the lines, breaking at line boundaries.
	for line in content:
		p = line.strip().split(' ')
		# json does not allow integer keys. Hence gotta do this
		pano_key = str( p[0] )
		if pano_key not in pano_dict:
			pano_dict[pano_key] = {}
			pano_dict[pano_key][ p[1] ] = p[2:]
		else:
			pano_dict[pano_key][ p[1] ] = p[2:]

	#print(pano_dict)
	#print("%%%%%%%%%%%%%%")
	#print( json.dumps( pano_dict ) )
	#for key in pano_dict:
	#	print( key )
	#	print( pano_dict[key] )
	#	print("")

def readBA_PositionCorrections(f):
	content = f.readlines()
	# Return a list of the lines, breaking at line boundaries.
	for line in content:
		p = line.strip().split(' ')
		# json does not allow integer keys. Hence gotta do this
		pano_key = str( p[0] )
		if pano_key not in pano_dict:
			pano_dict[pano_key] = {}
			pano_dict[pano_key][ "position" ] = [float(p[3]), float(p[2]), float(p[1])]
			pano_dict[pano_key][ "location" ] = [float(p[3]), float(p[1]), float(p[2])]
			pano_dict[pano_key][ "x-axis" ] = float(p[3])
			pano_dict[pano_key][ "y-axis" ] = float(p[1])
			pano_dict[pano_key][ "align_y" ] = float(p[5])
		#else:
		#	pano_dict[pano_key][ p[1] ] = p[2:]

	#print(pano_dict)
	#print("%%%%%%%%%%%%%%")
	#print( json.dumps( pano_dict ) )
	#for key in pano_dict:
	#	print( key )
	#	print( pano_dict[key] )
	#	print("")

def printEntry( record ):
	# id
	print( "id", record[1] )
	print( "node_number", record['node_number'])
	print( record['json_data'] )
	# x-axis, y-axis
	print( "x-axis", record[11], "y-axis", record[12])
	# align y
	print( "align_y", record[9] )
	print("")
	#for i, val in enumerate(record):
	#	print( i, val )
 
def main(args):
	my_args = args[1:]
	parsed_args, my_args = parse_args(my_args)

	homeId = parsed_args.homeId

	# Read the corrections from the Bundle Adjustment output
	readBA_PositionCorrections( parsed_args.positionCorrectionsFile )
	readBA_AngleCorrections( parsed_args.anglesCorrectionsFile )

	# First connect to the production db and copy the manually laid out entries (position, angle)
	# for all nodes in the house
	conn = None
	print("Connecting to production database -- Panoramas")

	try:
		# get a connection, if a connect cannot be made an exception will be raised here
		#conn = psycopg2.connect(conn_string)
		conn = psycopg2.connect(
				database="sf_db_prod",
  				user="sf_dev",
  				host="sf-rds-01.cfec0c0mvp7c.us-west-2.rds.amazonaws.com",
  				password="VirtualRealty12#$",
  				port="5432"
  				)
 
		# conn.cursor will return a cursor object, you can use this query to perform queries
		# note that in this example we pass a cursor_factory argument that will
		# dictionary cursor so COLUMNS will be returned as a dictionary so we
		# can access columns by their name instead of index.
		cursor_prod = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
 
		# execute our Query
		cursor_prod.execute("SELECT * FROM panoramas WHERE home_id=%(id)s", {'id':homeId})
		#cursor.execute("COPY (SELECT * FROM panoramas WHERE home_id=%(id)s) TO '/home/navneeth/dev/sf_ba_to_db/dump.csv' (format csv)", {'id':homeId})
 
		# retrieve the records from the database
		records_prod = cursor_prod.fetchall()		

		for record in records_prod:
			if record['node_number'] < 0:
				print("Warning invalid node number:", record['node_number'], " skipping")
				continue
		#	print(record['node_number'])
		#	the_dict = json.loads(record['json_data'])
		#	#print(the_dict)
		#	#print("\n")
		print("Writing prod database to file...")
		with open("P_beforeCorrection.csv", "wb") as csv_file:
			csv_writer = csv.writer(csv_file)
			csv_writer.writerow([i[0] for i in cursor_prod.description]) # write headers
			for row in records_prod:
				csv_writer.writerow(row)

		cursor_dev = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		# execute our Query
		cursor_dev.execute("SELECT * FROM panoramas_dev WHERE home_id=%(id)s", {'id':homeId})
		#cursor.execute("COPY (SELECT * FROM panoramas WHERE home_id=%(id)s) TO '/home/navneeth/dev/sf_ba_to_db/dump.csv' (format csv)", {'id':homeId})
 
		# retrieve the records from the database
		records_dev = cursor_dev.fetchall()		
		print("Writing dev database to file...")
		with open("Pdev_beforeCorrection.csv", "wb") as csv_file:
			csv_writer = csv.writer(csv_file)
			csv_writer.writerow([i[0] for i in cursor_dev.description]) # write headers
			for row in records_dev:
				csv_writer.writerow(row)



	# 	# Copy entries from the production db to the dev db
	# 	print("Copying entries from prod to dev...")
	# 	for record in records_prod:
	# 		#cursor_dev.execute("UPDATE panoramas_dev SET align_dir_x=%(align_x)s , align_dir_y=%(align_y)s, location_x=%(loc_x)s, location_y=%(loc_y)s, json_data = %(json)s WHERE home_id=%(id)s", {'id':homeId, 'align_x': record['align_dir_x'], 'align_y': record['align_dir_y'], 'loc_x': record['location_x'], 'loc_y': record['location_y'], 'json':record['json_data']} )
	# 		stmt = cursor_dev.mogrify("UPDATE panoramas_dev SET align_dir_x=%(align_x)s , align_dir_y=%(align_y)s, location_x=%(loc_x)s, location_y=%(loc_y)s, json_data = %(json)s WHERE home_id=%(id)s AND node_number=%(node_num)s", {'id':homeId, 'node_num': record['node_number'], 'align_x': record['align_dir_x'], 'align_y': record['align_dir_y'], 'loc_x': record['location_x'], 'loc_y': record['location_y'], 'json':record['json_data']} )
	# 		print(stmt)
	# 		try:
	# 			cursor_dev.execute(stmt)
	# 		except psycopg2.ProgrammingError as e:
	# 			conn.rollback()
	# 			# Err code lookup at http://www.postgresql.org/docs/9.1/static/errcodes-appendix.html
	# 			print("ERRRRORRRRR")
	# 			print("psycopg2 error code %s" % e.pgcode)
 	# 			#raise e
	# 		#else:
	# 	conn.commit()
	# 	print("Total number of rows updated :", cursor_dev.rowcount)


	# 	cursor_dev.execute("SELECT * FROM panoramas_dev WHERE home_id=%(id)s", {'id':homeId})
 
	# 	# retrieve the records from the database
	# 	records_dev = cursor_dev.fetchall()		
	# 	print("Writing dev database to file after copying ...")
	# 	with open("Pdev_afterCopy.csv", "wb") as csv_file:
	# 		csv_writer = csv.writer(csv_file)
	# 		csv_writer.writerow([i[0] for i in cursor_dev.description]) # write headers
	# 		for row in records_dev:
	# 			csv_writer.writerow(row)

	
	except psycopg2.DatabaseError as e:
	 	print("Error %s" % e)   
	 	sys.exit(1)
	
	finally:
	 	if conn:
	 		conn.close()

	################################################
	
	conn = None

	conn_string = "host='sf-rds-01.cfec0c0mvp7c.us-west-2.rds.amazonaws.com', dbname='sf_db_prod', user='sf_dev', password='VirtualRealty12#$'"
	# print the connection string we will use to connect
	print("Connecting to database\n	->%s" % (conn_string))

	try:
		# get a connection, if a connect cannot be made an exception will be raised here
		#conn = psycopg2.connect(conn_string)
		conn = psycopg2.connect(
				database="sf_db_prod",
  				user="sf_dev",
  				host="sf-rds-01.cfec0c0mvp7c.us-west-2.rds.amazonaws.com",
  				password="VirtualRealty12#$",
  				port="5432"
  				)
 
		# conn.cursor will return a cursor object, (dictionary cursor)
		# so COLUMNS will be returned as a dictionary so we
		# can access columns by their name instead of index.
		cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
 
 		homeId = parsed_args.homeId
		# execute our Query
		cursor.execute("SELECT * FROM panoramas_dev WHERE home_id=%(id)s", {'id':homeId})
 
		# retrieve the records from the database
		records = cursor.fetchall()

		print("%"*10)
		for record in records:
			if record['node_number'] < 0:
				print("Warning invalid node number:", record['node_number'], " skipping")
				continue
			#else:
			#	printEntry(record)

			#print(record['node_number'])
			# #the_dict = json.loads(record['json_data'])
			# #print(the_dict)
			# #print("\n")
			if str(record['node_number']) in pano_dict:
				print("Proposed Correction for node ", record['node_number'])
				printEntry(record)
				
				the_dict = json.loads(record['json_data'])

			 	# Uncomment to view BA proposed correction
				print("to ------>")
			 	print(pano_dict[ str(record['node_number']) ])

			 	# Position correction
			 	the_dict["position"] = pano_dict[ str(record['node_number']) ][ "position" ]
			 	the_dict["location"] = pano_dict[ str(record['node_number']) ][ "location" ]
			 	the_dict["x-axis"] = pano_dict[ str(record['node_number']) ][ "x-axis" ]
			 	the_dict["y-axis"] = pano_dict[ str(record['node_number']) ][ "y-axis" ]
			 	the_dict["align_y"] = pano_dict[ str(record['node_number']) ][ "align_y" ]

			# 	# Angle correction
			# 	for key in pano_dict[ str(record['node_number']) ]:
			# 		#print( "Looking for Pano:", key )
			# 		try:
			# 			location = the_dict['neighbors'].index( int(key) )
			# 		except ValueError:
			# 			location = -1
					 
			# 		delta_rotY = float( pano_dict[ str(record['node_number']) ][key][1] )
			# 		the_dict['neighbor_angles'][location] += delta_rotY 
					
			# 		#print( record['node_number'], '->', key, the_dict['neighbor_angles'][location], pano_dict[ str(record['node_number']) ][key][1])
			# 	print("to ------>")
			# 	print(the_dict)
			# 	#cursor.execute("UPDATE panoramas_dev SET json_data = %(json)s WHERE home_id=%(id)s AND node_number=%(node)s", {'id':homeId, 'node': record['node_number'], 'json':json.dumps(the_dict)})
			 	#stmt = cursor.mogrify("UPDATE panoramas_dev SET json_data = %(json)s WHERE home_id=%(id)s AND node_number=%(node)s", {'id':homeId, 'node': record['node_number'], 'json':json.dumps(the_dict)})
			 	stmt = cursor.mogrify("""
			 		UPDATE panoramas_dev 
			 		SET json_data = %(json)s , position = %(position)s, location = %(location)s, x-axis = %(xaxis)s, y-axis = %(yaxis)s, align_y = %(align_y)s
			 		WHERE home_id=%(id)s AND node_number=%(node)s
			 		""", {	'id':homeId, 'node': record['node_number'], 
			 				'position' : the_dict["position"],
			 				'location' : the_dict["location"],
			 				'xaxis' : the_dict["x-axis"],
			 				'yaxis' : the_dict["y-axis"],
			 				'align_y' : the_dict["align_y"],
			 				'json':json.dumps(the_dict)
			 				})
			 	print("SQL Query")
			 	print(stmt)
			 	try:
			 		cursor.execute(stmt)
			 	except psycopg2.IntegrityError:
			 		conn.rollback()
			 	else:
			 		conn.commit()
         			print "Total number of rows updated :", cursor.rowcount

     			#cursor.close()
     			print("-"*30)
		
	
	except psycopg2.DatabaseError as e:
		print("Error %s" % e)   
		sys.exit(1)
	
	finally:
		if conn:
			conn.close()

	print("******Closed db connection **********")
	return

	'''
	try:
		# get a connection, if a connect cannot be made an exception will be raised here
		#conn = psycopg2.connect(conn_string)
		conn = psycopg2.connect(
				database="sf_db_prod",
  				user="sf_dev",
  				host="sf-rds-01.cfec0c0mvp7c.us-west-2.rds.amazonaws.com",
  				password="VirtualRealty12#$",
  				port="5432"
  				)
 
		# conn.cursor will return a cursor object, you can use this query to perform queries
		# note that in this example we pass a cursor_factory argument that will
		# dictionary cursor so COLUMNS will be returned as a dictionary so we
		# can access columns by their name instead of index.
		cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
 
 		homeId = parsed_args.homeId
		# execute our Query
		cursor.execute("SELECT * FROM panoramas_dev WHERE home_id=%(id)s", {'id':homeId})
 
		# retrieve the records from the database
		records = cursor.fetchall()

		for record in records:
			if str(record['node_number']) in pano_dict:
				print(record['node_number'])
				the_dict = json.loads(record['json_data'])
				print(the_dict)
				print("\n")
	
	except psycopg2.DatabaseError as e:
		print("Error %s" % e)   
		sys.exit(1)
	'''
	 
if __name__ == "__main__":
	sys.exit( main(sys.argv) )
