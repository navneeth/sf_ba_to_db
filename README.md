Script for reading node-wise correction result from Bundle adjustment and applying it to a layout stored in the DB. 

Usage
=========================

Run using the script run.sh:

	./run.sh 

Arguments are:

--homeId -> ID of the home corresponding to the Bundle Adjustment file 
--correctionsFile -> File containing the node-wise corrections from a Bundle Adjustment run 

Format is as:

	162 163 -0.0992845 7.59466 -11.3627 1.6029 -7.42522 11.4733
	162 171 -2.15551 3.75124 -1.41343 2.252 -3.69418 1.55666
	163 171 -1.2589 -4.16832 9.70002 1.94721 3.89502 -9.81206
	fromNode toNode alpha1 alpha2 alpha3 beta1 beta2 beta3

commandline string to execute correction on homeId 20 with corrections file - 209_undertaker_vision_corrections.txt is:

	python connectToDB.py --homeId 20 --correctionsFile 209_undertaker_vision_corrections.txt